package com.citi.trading;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.Date;

import org.hamcrest.collection.IsMapContaining;
import org.junit.Test;

import com.citi.trading.Investor.NotificationHandler;

public class InvestorTest {

	public Date date = new Date();

	public static final String MRK_NOTIFICATION = "Trade: [2019-07-09 17:46:47.079, buy 100 MRK @ 60.0000]";

	public static class MockMarket implements OrderPlacer {
		public int nextID = 1;
		public Map<Integer, Consumer<Trade>> callbacks = new HashMap<>();
		public Map<Integer, Trade> orders = new HashMap<>();

	
		public synchronized void placeOrder(Trade order, Consumer<Trade> callback) {
			int orderID = nextID++;
			callbacks.put(orderID, callback);
			orders.put(orderID, order);
		}
	}

	@Test
	public void testBuyStock() {
		MockMarket mockMarket = new MockMarket();
		Investor investor = new Investor(10000, mockMarket);
		NotificationHandler callback = investor.getNotificationHandler();

		investor.buy("APPLE", 100, 20);
		Trade marketResponse = new Trade(new Timestamp(date.getTime()), "APPLE", true, 100, 20);

		callback.accept(marketResponse);

		Map<String, Integer> portfolio = investor.getPortfolio();
		double cash = investor.getCash();

		assertThat(cash, equalTo(8000.0));
		assertThat(portfolio.size(), is(1));
		assertThat(portfolio, IsMapContaining.hasEntry("APPLE", 100));
	}

	@Test
	public void testSellStock() {
		MockMarket mockMarket = new MockMarket();
		Map<String, Integer> portfolio = new HashMap<String, Integer>();
		portfolio.put("GOOGLE", 40);

		Investor investor = new Investor(portfolio, 10000, mockMarket);
		NotificationHandler callback = investor.getNotificationHandler();

		investor.sell("GOOGLE", 20, 100);
		Trade marketResponse = new Trade(new Timestamp(date.getTime()), "GOOGLE", false, 20, 100);

		callback.accept(marketResponse);

		Map<String, Integer> portfolioUpdated = investor.getPortfolio();
		double cash = investor.getCash();

		assertThat(cash, equalTo(12000.0));
		assertThat(portfolioUpdated.size(), is(1));
		assertThat(portfolioUpdated, IsMapContaining.hasEntry("GOOGLE", 20));
		assertThat(portfolioUpdated, not(IsMapContaining.hasEntry("APPLE", 20)));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testIllegalArgumentInput() {
		Map<String, Integer> portfolio = new HashMap<String, Integer>();

		portfolio.put("CITI", -20);
		new Investor(portfolio, 1000);
	}

	@Test
	public void testEmptyPortfolio() {
		MockMarket mockMarket = new MockMarket();
		Map<String, Integer> portfolio = new HashMap<String, Integer>();
		portfolio.put("GOOGLE", 40);

		Investor investor = new Investor(portfolio, 10000, mockMarket);
		NotificationHandler callback = investor.getNotificationHandler();

		investor.sell("GOOGLE", 40, 100);
		Trade marketResponse = new Trade(new Timestamp(date.getTime()), "GOOGLE", false, 40, 100);

		callback.accept(marketResponse);

		Map<String, Integer> portfolioUpdated = investor.getPortfolio();
		double cash = investor.getCash();

		assertThat(cash, equalTo(14000.0));
		assertThat(portfolioUpdated.size(), is(1));
		assertThat(portfolioUpdated, IsMapContaining.hasEntry("GOOGLE", 0));
	}

	@Test
	public void testBuyAndSellStock() {
		MockMarket mockMarket = new MockMarket();
		Map<String, Integer> portfolio = new HashMap<String, Integer>();
		portfolio.put("GOOGLE", 40);
		portfolio.put("APPLE", 150);

		Investor investor = new Investor(portfolio, 10000, mockMarket);
		NotificationHandler callback = investor.getNotificationHandler();

		investor.sell("GOOGLE", 20, 100);
		Trade marketResponseSell = new Trade(new Timestamp(date.getTime()), "GOOGLE", false, 20, 100);

		callback.accept(marketResponseSell);

		investor.buy("APPLE", 20, 15.2);
		Trade marketResponseBuy = new Trade(new Timestamp(date.getTime()), "APPLE", true, 20, 15.2);

		callback.accept(marketResponseBuy);

		Map<String, Integer> portfolioUpdated = investor.getPortfolio();
		double cash = investor.getCash();

		assertThat(cash, equalTo(11696.0));
		assertThat(portfolioUpdated.size(), is(2));
		assertThat(portfolioUpdated, IsMapContaining.hasEntry("GOOGLE", 20));
		assertThat(portfolioUpdated, IsMapContaining.hasEntry("APPLE", 170));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCannotAffordStock() {
		MockMarket mockMarket = new MockMarket();

		Investor investor = new Investor(1000, mockMarket);

		investor.buy("GOOGLE", 40, 100);
	}

}
