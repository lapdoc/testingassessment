package com.citi.trading;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

/**
 * Encapsulates a stock investor with a portfolio of stocks, some cash,
 * and the ability to place trade orders.
 * 
 * @author Will Provost
 */
public class Investor {

	private Map<String,Integer> portfolio;
	private double cash;
	private OrderPlacer market;

	/**
	 * Handler for trade confirmations.
	 */
	public class NotificationHandler implements Consumer<Trade> {
		public void accept(Trade trade) {
			synchronized(Investor.this) {
				String stock = trade.getStock();
				if (trade.isBuy()) {
					if (!portfolio.containsKey(stock)) {
						portfolio.put(stock, 0);
					}
					portfolio.put(stock, portfolio.get(stock) + trade.getSize());
					cash -= trade.getPrice() * trade.getSize();
				} else {
					portfolio.put(stock, portfolio.get(stock) - trade.getSize());
					cash += trade.getPrice() * trade.getSize();
				}
			}
		}
	}
	private NotificationHandler handler = new NotificationHandler();
	
	/**
	 * Create an investor with some cash but no holdings.
	 */
	public Investor(double cash) {
		this(new HashMap<>(), cash);
	}
	
	/**
	 * Create an investor with some cash on hand, an empty portfolio and
	 * the provided market object
	 */
	public Investor(double cash, OrderPlacer market) {
		checkCash(cash);
		this.portfolio = new HashMap<String, Integer>();
		this.cash = cash;
		this.market = market;
	}
	
	/**
	 * Create an investor with holdings as a map of tickers and share counts,
	 * and some cash on hand.
	 */
	public Investor(Map<String,Integer> portfolio, double cash) {
		checkCash(cash);
		for (int shares : portfolio.values()) {
			if (shares <= 0) {
				throw new IllegalArgumentException("All share counts must be positive.");
			}
		}
		
		this.portfolio = portfolio;
		this.cash = cash;
	}
	
	private boolean checkCash(double cash) {
		if(cash > 0) {
			return true;
		} else {
			throw new IllegalArgumentException("Starting cash cannot be negative");
		}
	}
	
	/**
	 * Create an investor with holdings as a map of tickers and share counts,
	 * and some cash on hand, and the market object provided
	 */
	public Investor(Map<String,Integer> portfolio, double cash, OrderPlacer market) {
		checkCash(cash);
		for (int shares : portfolio.values()) {
			if (shares <= 0) {
				throw new IllegalArgumentException("All share counts must be positive.");
			}
		}
		
		this.portfolio = portfolio;
		this.cash = cash;
		this.market = market;
	}
	
	/**
	 * Accessor for the portfolio.
	 */
	public Map<String,Integer> getPortfolio() {
		return portfolio;
	}
	
	/**
	 * Accessor for current cash. 
	 */
	public double getCash() {
		return cash;
	}
	
	/**
	 * Places an order to buy the given stock.
	 */
	public synchronized void buy(String stock, int size, double price) {
		if(size * price > this.getCash()) {
			throw new IllegalArgumentException("You do not have enough cash to make this offer");
		}
		
		Trade trade = new Trade(stock, true, size, price);
		market.placeOrder(trade, handler);
	}
	
	/**
	 * Places an order to sell the given stock.
	 */
	public synchronized void sell(String stock, int size, double price) {
		Trade trade = new Trade(stock, false, size, price);
		market.placeOrder(trade, handler);
	}
	
	public NotificationHandler getNotificationHandler() {
		return this.handler;
	}
	
	/**
	 * Quick test of the Investor class.
	 */
	public static void main(String[] args) {
		Investor investor = new Investor(10000);
		investor.buy("MRK", 100, 60);
		System.out.println("Placed order ...");
		try {
			Thread.sleep(15000);
		} catch (InterruptedException ex) {
			ex.printStackTrace();
		}
		
		System.out.println(investor.getPortfolio());
		System.out.println(investor.getCash());
		
		System.exit(0);
	}
}
